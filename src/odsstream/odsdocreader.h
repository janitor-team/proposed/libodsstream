/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once

#include <quazip5/quazip.h>

#include "reader/odscell.h"
#include "odsdochandlerinterface.h"

class SaxHandlerContentXml;

class OdsDocReader
{
  friend SaxHandlerContentXml;

  public:
  /**
   * creates an ODS reader with a reader handler
   *
   * @Param OdsDocHandlerInterface & the interface to implement to read ODS
   * files
   */

  OdsDocReader(OdsDocHandlerInterface &handler);
  virtual ~OdsDocReader();


  void parse(QFile &odsFile);
  void parse(QIODevice *p_inputstream);


  private:
  void setInsideCell(const OdsCell &cell);
  void startInsideLine();

  void startSheet(const QString &sheet_name);

  void endSheet();

  void startLine();

  void endLine();

  void setCell(const OdsCell &cell);
  void endDocument();


  QuaZip *_p_quaZip = nullptr;

  uint _column_number;

  OdsDocHandlerInterface &_handler;
};
