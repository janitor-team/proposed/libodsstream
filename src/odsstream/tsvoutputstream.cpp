/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "tsvoutputstream.h"

TsvOutputStream::TsvOutputStream(QTextStream &otxtstream) : TsvDirectoryWriter()
{
  _p_otxtstream = &otxtstream;
}

TsvOutputStream::~TsvOutputStream()
{
  // close();
  _p_otxtstream = nullptr;
}

void
TsvOutputStream::close()
{
  _p_otxtstream->flush();
}

void
TsvOutputStream::setNoSheetName(bool no_sheet_name)
{
  _no_sheet_name = no_sheet_name;
}

void
TsvOutputStream::writeSheet(const QString &sheetName)
{
  if(_no_sheet_name)
    return;
  *_p_otxtstream << _end_of_line << _end_of_line << "**** " << sheetName
                 << " ****" << _end_of_line;
}
