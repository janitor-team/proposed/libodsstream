/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once

#include <QException>
#include <QDebug>


class OdsException : public QException
{
  public:
  OdsException(const QString &message) throw()
  {
    qDebug() << message;
    m_message = message;
    m_stdMessage.assign(m_message.toStdString());
  }

  OdsException(const OdsException &other) throw()
  {
    m_message = other.m_message;
    m_stdMessage.assign(m_message.toStdString());
  }
  void
  raise() const override
  {
    throw *this;
  }
  virtual QException *
  clone() const override
  {
    return new OdsException(*this);
  }

  virtual const QString &
  qwhat() const throw()
  {
    return m_message;
  }

  const char *
  what() const noexcept override
  {
    return m_stdMessage.c_str();
  }

  virtual ~OdsException() throw()
  {
  }

  private:
  std::string m_stdMessage;
  QString m_message; // Description of the error
};
