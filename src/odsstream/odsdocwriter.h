/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once

#include <QIODevice>
#include <QString>
#include <QUrl>

#include <quazip5/quazip.h>
#include "writer/structure/contentxml.h"
#include "calcwriterinterface.h"
#include "writer/structure/settingsxml.h"

class OdsDocWriter : public CalcWriterInterface
{
  public:
  OdsDocWriter(const QString &filename);
  OdsDocWriter(QIODevice *device);
  virtual ~OdsDocWriter();

  void close() override;

  void writeSheet(const QString &sheetName) override;
  void writeLine() override;
  void writeCell(const char *) override;
  void writeCell(const QString &) override;
  void writeEmptyCell() override;
  void writeCell(std::size_t) override;
  void writeCell(int) override;
  void writeCell(float) override;
  void writeCell(double) override;
  void writeCellPercentage(double value) override;
  void writeCell(bool) override;
  void writeCell(const QDate &) override;
  void writeCell(const QDateTime &) override;
  void writeCell(const QUrl &, const QString &) override;
  OdsTableCellStyleRef
  getTableCellStyleRef(const OdsTableCellStyle &style) override;
  void setTableCellStyleRef(OdsTableCellStyleRef style_ref) override;
  void setCellAnnotation(const QString &annotation) override;
  void addColorScale(const OdsColorScale &ods_color_scale) override;
  QString getOdsCellCoordinate() override;

  void setCurrentOdsTableSettings(const OdsTableSettings &settings) override;

  private:
  void openDevice(QIODevice *device);
  void clearAnnotation();

  private:
  ContentXml *_p_content;
  QuaZip *_p_quaZip;
  QIODevice *_p_device  = nullptr;
  bool _p_device_delete = false;
  QString _next_annotation;
  SettingsXml _settings_xml;
};
