/**
 * \file odsstream/tsvreader.h
 * \date 11/11/2018
 * \author Olivier Langella
 * \brief parser for text files (tsv, csv)
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the libodsstream library.
 *
 *    libodsstream is a library to read and write ODS documents as streams
 *    Copyright (C) 2018  Olivier Langella <Olivier.Langella@u-psud.fr>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *by the Free Software Foundation, either version 3 of the License, or (at your
 *option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QFile>
#include <QTextStream>
#include "config.h"
#include "reader/odscell.h"
#include "odsdochandlerinterface.h"


class TsvReader
{

  private:
  std::size_t m_columnNumber=0;
  std::size_t m_rowNumber=0;

  OdsDocHandlerInterface &m_handler;

  QChar m_separator = '\t';

  public:
  /**
   * creates an ODS reader with a reader handler
   *
   * @Param OdsDocHandlerInterface & the interface to implement to read ODS
   * files
   */

  TsvReader(OdsDocHandlerInterface &handler);
  virtual ~TsvReader();


  void parse(QFile &tsvFile);
  void parse(QIODevice *p_inputstream);
  void parse(QTextStream &in);
  
  void setSeparator(TsvSeparator separator);


  private:
  /** @brief reads a CSV row
   * based on
   * https://stackoverflow.com/questions/27318631/parsing-through-a-csv-file-in-qt
   */
  bool readCsvRow(QTextStream &in);
};
