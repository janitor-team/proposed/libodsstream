/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once
#include <quazip5/quazip.h>
#include <QXmlStreamWriter>
#include <QHash>

class StylesXml
{

  public:
  StylesXml(QuaZip *p_quaZip);
  virtual ~StylesXml();
  static const QString &getNamespaceURI(const QString &xml_namespace);

  private:
  static QHash<QString, QString> *_p_hashNamespaceURI;
  void WriteHeader(QXmlStreamWriter &writer);
  void WriteFontFaceDecls(QXmlStreamWriter &writer);
  void WriteOfficeStyles(QXmlStreamWriter &writer);
  void WriteAutomaticStyles(QXmlStreamWriter &writer);
  void WriteMasterStyles(QXmlStreamWriter &writer);
};
