/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once

#include <quazip5/quazip.h>
#include <quazip5/quazipfile.h>
#include <QXmlStreamWriter>
#include <QUrl>
#include <QDate>
#include <QDateTime>
#include "../options/odstablecellstyle.h"
#include "../options/odstablecellstyleref.h"
#include "../options/odscolorscale.h"
#include "settingsxml.h"


class ContentXml
{
  public:
  ContentXml(QuaZip *p_quaZip, SettingsXml *p_settings_xml);
  virtual ~ContentXml();
  void writeSheet(const QString &sheetName);
  void writeCell(double value, const QString &annotation);
  void writeCell(std::size_t value, const QString &annotation);
  void writeCell(int value, const QString &annotation);
  void writeCell(const QString &value, const QString &annotation);
  void writeCell(const QDate &, const QString &annotation);
  void writeCell(const QDateTime &, const QString &annotation);
  void writeCell(const QUrl &, const QString &, const QString &annotation);
  void writeCell(bool value, const QString &annotation);
  void writeCellPercentage(double value, const QString &annotation);
  void writeEmptyCell(const QString &annotation);
  void writeLine();
  void close();

  OdsTableCellStyleRef getTableCellStyleRef(const OdsTableCellStyle &style);
  void setTableCellStyleRef(OdsTableCellStyleRef style_ref);
  void addColorScale(const OdsColorScale &ods_color_scale);
  QString getCellCoordinate();

  private:
  void writeCellFloat(const QString &value,
                      const QString &representation,
                      const QString &annotation);
  void writeSheet();
  void writeEndTable();
  std::vector<OdsColorScale>
  getOdsColorScaleListBySheetName(const QString &sheet_name);

  void WriteHeader();
  void writeFontFaceDecls();
  void writeAutomaticStyles();
  void startSpreadsheet();
  void writeAnnotation(const QString &annotation);

  QuaZipFile _outFile;

  static QString _xsdNamespaceURI;
  static QString _xsiNamespaceURI;


  QuaZip *_p_quaZip;
  QXmlStreamWriter *_p_writer;

  bool _tableStarted;

  bool _tableRowStarted;
  bool _automatic_styles_writed = false;
  bool _spreadsheet_started     = false;
  std::vector<OdsTableCellStyle> _style2write;
  std::vector<OdsTableCellStyleRef> _style_ref_list;
  OdsTableCellStyleRefInternal *_current_style_ref = nullptr;

  std::vector<OdsColorScale> _color_scale_list;
  QString _current_sheet_name;


  unsigned int _row_pos = 0;
  unsigned int _col_pos = 0;
  SettingsXml *_p_settings_xml;
};
