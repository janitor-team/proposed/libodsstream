/**
 * \file odsstream/writer/structure/settings.h
 * \date 20/04/2018
 * \author Olivier Langella
 * \brief XML file to store ODS document settings
 */

/*******************************************************************************
 * Copyright (c) 2013 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the libodsstream library.
 *
 *    libodsstream is a library to read and write ODS documents as streams
 *    Copyright (C) 2013  Olivier Langella <Olivier.Langella@u-psud.fr>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *by the Free Software Foundation, either version 3 of the License, or (at your
 *option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include <quazip5/quazip.h>
#include <QXmlStreamWriter>
#include "../options/odstablesettings.h"

class OdsDocWriter;
class ContentXml;

class SettingsXml
{
  friend OdsDocWriter;
  friend ContentXml;

  protected:
  SettingsXml();
  virtual ~SettingsXml();

  void addSheetName(const QString &name);
  const QString getDefaultSheetName() const;

  void setCurrentOdsTableSettings(const OdsTableSettings &settings);

  void write(QuaZip *p_quazip);

  private:
  void writeConfigurationSettings(QXmlStreamWriter *p_writer);
  void writeSheetSettings(QXmlStreamWriter *p_writer,
                          const QString &sheet_name);
  void writeConfigItem(QXmlStreamWriter *p_writer,
                       const QString &name,
                       const QString &type,
                       const QString &value);

  private:
  OdsTableSettings _default_table_settings;

  std::vector<QString> _sheet_list;

  std::vector<OdsTableSettings> _table_settings_list;
};
