/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "manifestxml.h"
#include <QXmlStreamWriter>

#include <quazip5/quazipfile.h>

QString ManifestXml::_namespaceURI =
  "urn:oasis:names:tc:opendocument:xmlns:manifest:1.0";

ManifestXml::ManifestXml(QuaZip *p_quaZip) : _p_quaZip(p_quaZip)
{
  QuaZipFile outFile(_p_quaZip);
  QuaZipNewInfo info("META-INF/manifest.xml");
  outFile.open(QIODevice::WriteOnly, info);
  _p_writer = new QXmlStreamWriter(&outFile);
  _p_writer->setAutoFormatting(true);

  this->WriteHeader();

  _p_writer->writeStartElement(_namespaceURI, "file-entry");
  _p_writer->writeAttribute(_namespaceURI, "full-path", "/");
  _p_writer->writeAttribute(_namespaceURI,
                            "media-type",
                            "application/vnd.oasis.opendocument.spreadsheet");
  _p_writer->writeEndElement();

  _p_writer->writeStartElement(_namespaceURI, "file-entry");
  _p_writer->writeAttribute(_namespaceURI, "full-path", "styles.xml");
  _p_writer->writeAttribute(_namespaceURI, "media-type", "text/xml");
  _p_writer->writeEndElement();

  _p_writer->writeStartElement(_namespaceURI, "file-entry");
  _p_writer->writeAttribute(_namespaceURI, "full-path", "content.xml");
  _p_writer->writeAttribute(_namespaceURI, "media-type", "text/xml");
  _p_writer->writeEndElement();

  _p_writer->writeStartElement(_namespaceURI, "file-entry");
  _p_writer->writeAttribute(_namespaceURI, "full-path", "meta.xml");
  _p_writer->writeAttribute(_namespaceURI, "media-type", "text/xml");
  _p_writer->writeEndElement();

  //<manifest:file-entry manifest:full-path="mimetype"
  // manifest:media-type="text/plain" />
  /*
  _p_writer->writeStartElement(_namespaceURI, "file-entry");
  _p_writer->writeAttribute(_namespaceURI, "full-path", "mimetype");
  _p_writer->writeAttribute(_namespaceURI, "media-type", "text/plain");
  _p_writer->writeEndElement();
*/
  //<manifest:file-entry manifest:full-path="META-INF/manifest.xml"
  //		manifest:media-type="text/xml" />
  /*
  _p_writer->writeStartElement(_namespaceURI,"file-entry");
  _p_writer->writeAttribute(_namespaceURI, "full-path",
  "META-INF/manifest.xml"); _p_writer->writeAttribute(_namespaceURI,
  "media-type", "text/xml"); _p_writer->writeEndElement();
  */

  //<manifest:file-entry manifest:full-path="settings.xml"
  //manifest:media-type="text/xml"/>
  _p_writer->writeStartElement(_namespaceURI, "file-entry");
  _p_writer->writeAttribute(_namespaceURI, "full-path", "settings.xml");
  _p_writer->writeAttribute(_namespaceURI, "media-type", "text/xml");
  _p_writer->writeEndElement();


  _p_writer->writeEndDocument();

  delete _p_writer;

  outFile.close();
}

ManifestXml::~ManifestXml()
{
}

void
ManifestXml::WriteHeader()
{
  _p_writer->writeStartDocument("1.0");
  //<manifest:manifest
  //xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0"
  //manifest:version="1.2" >
  _p_writer->writeNamespace(_namespaceURI, "manifest");
  _p_writer->writeStartElement(_namespaceURI, "manifest");

  _p_writer->writeAttribute(_namespaceURI, "version", "1.2");
  //_p_writer->setPrefix("manifest", namespaceURI);
  //_p_writer->setDefaultNamespace(namespaceURI);
  //_p_writer->writeStartElement("mzXML");

  //_p_writer->writeAttribute(xmlnsxsi, "schemaLocation", xsischemaLocation);


  // not compatible with MS office 2010 :
  //_p_writer->writeAttribute("manifest", namespaceURI, "version", "1.2");
}
