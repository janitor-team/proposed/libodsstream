/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once

#include <quazip5/quazip.h>
#include <QXmlStreamWriter>
#include <map>

class MetaXml
{

  public:
  MetaXml(QuaZip *p_quaZip);
  virtual ~MetaXml();

  static const QString &getNamespaceURI(const QString &xml_namespace);
  static std::map<const QString, const QString> _hash_namespace_uri;

  private:
  void WriteHeader(QXmlStreamWriter *p_writer);

  QuaZip *_p_quaZip;
};
