/**
 * \file odsstream/writer/options/odstablesettings.cpp
 * \date 23/04/2018
 * \author Olivier Langella
 * \brief store table settings
 */

/*******************************************************************************
 * Copyright (c) 2013 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the libodsstream library.
 *
 *    libodsstream is a library to read and write ODS documents as streams
 *    Copyright (C) 2013  Olivier Langella <Olivier.Langella@u-psud.fr>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *by the Free Software Foundation, either version 3 of the License, or (at your
 *option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "odstablesettings.h"
#include <QDebug>

OdsTableSettings::OdsTableSettings()
{
}
OdsTableSettings::OdsTableSettings(const OdsTableSettings &other)
{
  _sheetname      = other._sheetname;
  _map_properties = other._map_properties;
}
OdsTableSettings::~OdsTableSettings()
{
}
OdsTableSettings &
OdsTableSettings::operator=(OdsTableSettings const &other)
{
  if(this == &other)
    return *this;
  _sheetname      = other._sheetname;
  _map_properties = other._map_properties;
  return *this;
}
void
OdsTableSettings::setSheetName(const QString &sheetname)
{
  _sheetname = sheetname;
}
const QString &
OdsTableSettings::getSheetName() const
{
  return _sheetname;
}
const QString
OdsTableSettings::getValue(const QString &property) const
{
  auto it = _map_properties.find(property);
  if(it != _map_properties.end())
    {
      return it->second;
    }
  else
    {
      return "0";
    }
}
void
OdsTableSettings::setVerticalSplit(unsigned int position)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << position;
  if(position == 0)
    {
      _map_properties[QString("VerticalSplitMode")] = QString("0");
    }
  else
    {
      _map_properties[QString("VerticalSplitMode")] = QString("2");
    }
  _map_properties[QString("VerticalSplitPosition")] =
    QString("%1").arg(position);
  qDebug() << __FILE__ << " " << __FUNCTION__ << " "
           << getValue("VerticalSplitPosition");
}

void
OdsTableSettings::setHorizontalSplit(unsigned int position)
{
  if(position == 0)
    {
      _map_properties[QString("HorizontalSplitMode")] = QString("0");
    }
  else
    {
      _map_properties[QString("HorizontalSplitMode")] = QString("2");
    }
  _map_properties[QString("HorizontalSplitPosition")] =
    QString("%1").arg(position);
}
