/**
 * \file odsstream/qtablewriter.h
 * \date 15/03/2019
 * \author Olivier Langella
 * \brief write an entire table sheet from a QAbstractModel
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the libodsstream library.
 *
 *    libodsstream is a library to read and write ODS documents as streams
 *    Copyright (C) 2019  Olivier Langella <Olivier.Langella@u-psud.fr>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *by the Free Software Foundation, either version 3 of the License, or (at your
 *option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "calcwriterinterface.h"
#include <QAbstractProxyModel>
#include <map>

class QtableWriter
{
  private:
  CalcWriterInterface *mp_writer;
  const QAbstractProxyModel *mp_tableModel;
  std::map<int, bool> m_percentColumns;


  public:
  QtableWriter(CalcWriterInterface *p_writer,
               const QAbstractProxyModel *p_table_model);

  /**
   * \brief write the entire table in a sheet
   *
   * \param sheetName the sheet name
   */
  void writeSheet(const QString &sheetName);

  void setFormatPercentForColumn(const QString &column_title);
  void setFormatPercentForColumn(const QModelIndex &column_index);
};
