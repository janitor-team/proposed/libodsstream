/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once


#include <QString>
#include <QUrl>
#include <QDate>
#include <QDir>
#include <QTextStream>
#include <QFile>
#include "calcwriterinterface.h"
#include "config.h"

class TsvDirectoryWriter : public CalcWriterInterface
{
  public:
  TsvDirectoryWriter(const QDir &directory);
  virtual ~TsvDirectoryWriter();


  void close() override;

  void writeSheet(const QString &sheetName) override;
  void writeLine() override;
  void writeCell(const char *) override;
  void writeCell(const QString &) override;
  void writeEmptyCell() override;
  void writeCell(std::size_t) override;
  void writeCell(int) override;
  void writeCell(float) override;
  void writeCell(double) override;
  void writeCellPercentage(double value) override;
  void writeCell(bool) override;
  void writeCell(const QDate &) override;
  void writeCell(const QDateTime &) override;
  void writeCell(const QUrl &, const QString &) override;
  void setCellAnnotation([[maybe_unused]] const QString &annotation) override{};


  /** @brief sets the separator to use between values (cells)
   *
   * @param TsvSeparator enumeration to choose between possible choices
   */
  void setSeparator(TsvSeparator separator);

  /** @brief get the separator used between values (cells)
   *
   * @return TsvSeparator used
   */
  TsvSeparator getSeparator() const;

  /** @brief set a flag to quote strings
   *
   * @param bool if true, strings will be quoted, choose false otherwise
   * @return bool state of the modified flag
   */
  bool setQuoteStrings(bool quote_strings);


  /** @brief tells if the quote string flag is enabled
   */
  bool isQuoteStrings() const;


  /** @brief enable a physical flush on device at each new line
   * enables this if you want to ensure that each line is written. This costs a
   * little overhead
   *
   * @param bool if true, lines will be flushed, choose false otherwise
   * @return bool state of the modified flag

   * */
  bool setFlushLines(bool flushOk);

  /** @brief tells if the flush lines flag is enabled
   */
  bool isFlushLines() const;

  protected:
  TsvDirectoryWriter();
  void writeRawCell(const QString &text);

  QString m_separator            = "\t";
  QString _end_of_line           = "\n";
  QTextStream *_p_otxtstream     = nullptr;
  unsigned int numFloatPrecision = 12;

  private:
  const QDir _directory;

  QString _file_extension = ".tsv";

  bool _tableRowStart = true;
  bool _startingSheet = false;
  bool m_quoteStrings = false;
  bool m_flushLines   = false;
  TsvSeparator m_tsvSeparatorEnum = TsvSeparator::tab;

  QFile *_p_ofile = nullptr;
  void ensureSheet();
};
