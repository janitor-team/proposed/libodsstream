/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once

#include <QString>
#include <QDateTime>
//#include <QDebug>
class SaxHandlerContentXml;
class TsvReader;

class OdsCell
{
  friend SaxHandlerContentXml;
  friend TsvReader;

  public:
  OdsCell();
  virtual ~OdsCell();

  const QString &toString() const;
  const QString &getOfficeValueType() const;
  const QDateTime &getDateTimeValue() const;
  const QString &getStringValue() const;
  bool getBooleanValue() const;
  double getDoubleValue() const;
  bool isBoolean() const;
  bool isDate() const;
  bool isDouble() const;
  bool isString() const;
  bool isEmpty() const;

  protected:
  void setOfficeValueType(const QString &type);
  void setDateValue(const QDateTime &date);
  void setValueString(const QString &value);
  void setValueDouble(double value_num);
  void setValueBoolean(bool value_bool);
  void setValueOfUndefinedType(const QString &value);

  QString _string_value;
  QString _office_value_type;
  QDateTime _date_value;
  double _double_value;
  bool _bool_value;
  bool _is_empty;
};



