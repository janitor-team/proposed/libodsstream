message("\n${BoldRed}WIN10-MINGW64 environment${ColourReset}\n")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

set(MAKE_TEST 0)

if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()


set(QUAZIP_FOUND 1)
set(QUAZIP_INCLUDE_DIR "${HOME_DEVEL_DIR}/quazip5/development")
set(QUAZIP_LIBRARIES "${HOME_DEVEL_DIR}/quazip5/build-area/mingw64/libquazip5.dll") 
if(NOT TARGET QuaZip::QuaZip)
	add_library(QuaZip::QuaZip UNKNOWN IMPORTED)
	set_target_properties(QuaZip::QuaZip PROPERTIES
		IMPORTED_LOCATION             "${QUAZIP_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QUAZIP_INCLUDE_DIR}")
endif()
message(STATUS "Found QUAZIP: ${QUAZIP_LIBRARIES}")


find_package(ZLIB REQUIRED)


# On Win10 all the code is relocatable.
remove_definitions(-fPIC)
