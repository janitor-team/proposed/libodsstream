message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)


if(NOT CMAKE_INSTALL_PREFIX)
	set(CMAKE_INSTALL_PREFIX /usr/local)
endif(NOT CMAKE_INSTALL_PREFIX)


find_package(ZLIB REQUIRED)


find_package(QuaZip QUIET)
if(NOT QUAZIP_FOUND)
	message(STATUS "QuaZip not yet found. Searching for it.")
	set(QuaZip_DIR ${CMAKE_MODULE_PATH})
	find_package(QuaZip REQUIRED)
endif()



add_definitions(-fPIC)

