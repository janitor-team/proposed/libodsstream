# Copyright : Olivier Langella (CNRS)
# License : GPL-3.0+
# Authors : Olivier Langella

set(OdsStream_FOUND 1)
set(OdsStream_INCLUDE_DIRS ${CMAKE_INSTALL_PREFIX}/include/odsstream)
set(OdsStream_LIBRARY ${CMAKE_INSTALL_PREFIX}/lib${LIB_SUFFIX}/libodsstream.so.${LIBODSSTREAM_VERSION})

if(NOT TARGET OdsStream::Core)
	add_library(OdsStream::Core UNKNOWN IMPORTED)
	set_target_properties(OdsStream::Core PROPERTIES
		IMPORTED_LOCATION "${CMAKE_INSTALL_PREFIX}/lib${LIB_SUFFIX}/libodsstream.so.${LIBODSSTREAM_VERSION}"
		INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/include/odsstream"
		)
endif()
