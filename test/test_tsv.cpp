
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <QDebug>
#include <iostream>
#include <odsstream/odsexception.h>
#include <odsstream/tsvdirectorywriter.h>
#include <odsstream/tsvoutputstream.h>
#include <odsstream/writer/options/odscolorscale.h>
#include <odsstream/tsvreader.h>

using namespace std;

// make test ARGS="-V -I 2,2"

class CustomHandler : public OdsDocHandlerInterface
{
  public:
  /**
   * callback that indicates the begining of a data sheet. Override it in
   * order to retrieve information about the current data sheet.
   *
   */
  virtual void startSheet([[maybe_unused]] const QString &sheet_name){};

  /**
   * callback that indicates the end of the current data sheet. Override it if
   * needed
   */
  virtual void
  endSheet()
  {
    qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
             << " endSheet";
  };

  /**
   * callback that indicates a new line start. Override it if needed.
   */

  virtual void
  startLine()
  {
    qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
             << " startLine";
  };

  /**
   * callback that indicates a line ending. Override it if needed.
   */

  virtual void
  endLine()
  {
    qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
             << " endLine";
  };

  /**
   * callback that report the content of the current cell in a dedicated Cell
   * object. Override it if you need to retrieve cell content.
   */
  virtual void
  setCell(const OdsCell &cell) override
  {
    qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
             << cell.toString();
  };

  /**
   * callback that report the end of the ODS document. Override it if you need
   * to know that reading is finished.
   */
  virtual void endDocument(){};
};

TEST_CASE("TSV test suite.", "[TSV]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  // QCoreApplication a(argc, argv);
  SECTION("..:: Test TsvDirectoryWriter ::..", "[TSV]")
  {

    TsvDirectoryWriter writer(QDir("test"));
    
    writer.setFlushLines(true);

    QString test("truc");

    writer.writeSheet("classeur");
    writer.setCellAnnotation("test annot");
    writer.writeCell(test);
    writer.writeLine();
    writer.writeEmptyCell();
    writer.writeCell("coucou\" ceci est un quote");
    bool vf(0);
    writer.writeCell(vf);
    writer.writeCellPercentage(0.23565654545);

    writer.clearTableCellStyleRef();
    writer.writeLine();
    writer.writeLine();
    writer.writeLine();
    writer.writeCell(1);
    QString start_position = writer.getOdsCellCoordinate();
    writer.writeCell(2);
    writer.writeCell(3);
    writer.writeCell(4);
    writer.writeCell(5);

    writer.writeLine();
    writer.writeCell(6);
    writer.writeCell(7);
    writer.writeCell(8);
    writer.writeCell(9);
    writer.writeCell(10);
    QString end_position = writer.getOdsCellCoordinate();

    OdsColorScale color_scale(start_position, end_position);
    writer.addColorScale(color_scale);

    writer.writeSheet("classeur2");
    writer.setCellAnnotation("test annot");
    writer.writeCell(test);
    writer.writeLine();
    writer.writeEmptyCell();
    writer.writeCell("coucou");

    QDateTime currentdate(QDateTime::currentDateTime());

    writer.writeCell(currentdate);

    writer.close();

    QTextStream terminalOut(stdout, QIODevice::WriteOnly);
    TsvOutputStream writero(terminalOut);
    
    writero.setFlushLines(true);

    writero.writeSheet("classeur");
    writero.writeCell(test);
    writero.writeLine();
    writero.writeEmptyCell();
    writero.setCellAnnotation("ne sera pas prise en compte en TSV");
    writero.writeCell("coucou");
    writero.writeCell(vf);

    writero.writeCell(currentdate);

    writero.close();

    CustomHandler handler;
    TsvReader tsv_reader(handler);
    // tsv_reader.setSeparator(TsvSeparator::comma);
    QFile tsv_file("test/classeur.tsv");
    tsv_reader.parse(tsv_file);
  }
}
