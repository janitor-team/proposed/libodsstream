#include "config.h"
#include <QDebug>
#include <QFile>
#include <cstring>

using namespace std;

// make test ARGS="-V -I 3,3"

struct ZipLocalFileHeader
{
  quint32 header_signature;
  char version[2];
  char bit_flag[2];
  char compression_method[2];
  char last_modification_time[2];
  char last_modification_date[2];
  char crc[4];
  quint32 compressed_size    = 0;
  quint32 uncompressed_size  = 0;
  quint16 name_length        = 0;
  quint16 extra_field_length = 0;
};

bool
readZipHeaderBuffer(QIODevice *p_device, const QString &look_for)
{
  QString filename;


  ZipLocalFileHeader zip_header;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  char zip_header_buffer[30];
  if(p_device->atEnd())
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      return false;
    }
  p_device->read(zip_header_buffer, sizeof(char[30]));
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  if(p_device->atEnd())
    {
      qDebug() << "atEnd 1";
      return false;
    }

  std::strncpy(zip_header.crc, zip_header_buffer + 14, 4);
  zip_header.header_signature   = *(zip_header_buffer);
  zip_header.compressed_size    = *(zip_header_buffer + 18);
  zip_header.uncompressed_size  = *(zip_header_buffer + 22);
  zip_header.name_length        = *(zip_header_buffer + 26);
  zip_header.extra_field_length = *(zip_header_buffer + 28);


  char word[255];
  std::strncpy(word, zip_header.crc, 4);
  word[4] = '\0';
  qDebug() << "word=" << word;

  qDebug() << "header_signature=" << zip_header.header_signature;
  qDebug() << "filename length=" << zip_header.name_length;
  qDebug() << "compressed size=" << zip_header.compressed_size;
  qDebug() << "uncompressed size=" << zip_header.uncompressed_size;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  char one_byte;
  quint16 i = 0;
  while(i < zip_header.name_length)
    {
      p_device->read(&one_byte, sizeof(char));

      if(p_device->atEnd())
        {
          qDebug() << "atEnd 4";
          return false;
        }
      if(i < 254)
        {
          word[i] = one_byte;
        }
      else
        {
          word[254] = '\0';
        }

      i++;
    }
  if(i < 254)
    {
      word[i] = '\0';
    }


  filename = word;
  qDebug() << "filename=" << filename;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  p_device->read(word, sizeof(char[zip_header.extra_field_length]));

  if(p_device->atEnd())
    {
      qDebug() << "atEnd 3";
      return false;
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  i = 0;
  while(i < zip_header.extra_field_length)
    {
      p_device->read(&one_byte, sizeof(char));

      if(p_device->atEnd())
        {
          qDebug() << "atEnd 4";
          return false;
        }

      i++;
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  if(look_for == filename)
    return true;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  quint32 j = 0;
  while(j < zip_header.compressed_size)
    {
      p_device->read(&one_byte, sizeof(char));

      if(p_device->atEnd())
        {
          qDebug() << "atEnd 5";
          return false;
        }

      j++;
    }
  return false;
}

int
main([[maybe_unused ]] int argc, [[maybe_unused ]] char **argv)
{
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
  QFile file(QString(CMAKE_SOURCE_DIR).append("/test/data/xic.ods"));
  if(!file.open(QIODevice::ReadOnly))
    return 1;


  while(readZipHeaderBuffer(&file, "content") == false)
    {
      if(file.atEnd())
        {
          qDebug() << "atEnd main";
          break;
        }
      qDebug() << "ok main";
    }
  file.close();
  // SUCCESS
  return 0;
}
